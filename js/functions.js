var $api='http://opratel.local/',$cantnotifications=0;
function addCookie(cname,val,expire=1000*60*60*24*365){
    var d = new Date();d.setTime(d.getTime() + expire);
    var expires = "expires="+d.toUTCString();
    document.cookie=cname+"="+val+";"+expires+";path=/";
}
function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
}
function checkCookie($name) {
    var cookie=getCookie($name);
    if (cookie != "") {
        return cookie;
    }
}
function resetLoginCookies(){
    addCookie('log','0',0);
    addCookie('user','none',0);
    addCookie('type','guest',0);
    addCookie('ssid','',0);
}
function notification_close($id){
    $('#'+$id).remove();
}
function notification_add($text,$type){
    $cantnotifications++;
    var $id='notification'+$cantnotifications,$time=5000;
    if($(window).width()>767){
        $time+=10000;
    }
    $('#notifications').html('<div class="notification '+$type+'" id="'+$id+'" onclick="notification_close(\''+$id+'\')">'+$text+'</div>'+$('#notifications').html());
    setTimeout(function(){
        notification_close($id);
    },$time);
}
function check_login(){
    var $log=checkCookie('log'),$view='header.header-logged-';
    if($log=='true'){
        $view+=checkCookie('type');
    }else{
        $view+='out';
    }
    if($('#container').html()==''){
        load_view('home');
    }
    load_view($view,'header-nav');
}
function load_view($view,$target='container'){  
    if($('#'+$view).length>0){
        return null;
    }
    if($(window).width()<768){
        $('#header-nav').slideUp();
    }
    var $route=$view.toString().replace('.','/');
    var $url='../views/'+$route+'.html';
    $('#'+$target).html("");
    $('#'+$target).load($url);
}

window['login']=0;
window['sessionid']=checkCookie('ssid');
function do_login(){
    if(window['login']>0){
        notification_add('Estas en proceso de inicio de ses&oacute;n, espera un instante','error');
        return null;
    }
    var $user=$('#username').val(),$pass=$('#password').val();
    if($user=='' || $pass==''){
        notification_add('Completa los datos para iniciar sesion','error');
        return null;
    }
    window['login']++;
    $.get($api+'login',{user:$user,pass:$pass},function(result){
        var $result=JSON.parse(result);
        if($result['success']==true){
            resetLoginCookies();
            addCookie('log',true);
            addCookie('user',$result['messages']['user']);
            addCookie('type',$result['messages']['type']);
            addCookie('ssid',$result['messages']['sessionid']);
            notification_add($result['messages']['user']+' ingresaste correctamente.','success');
            window['sessionid']=checkCookie('ssid');
            check_login();
            load_view('home');
        }else{
            resetLoginCookies();
            notification_add($result['errors'][0],'error');
        }
        window['login']=0;
    }).fail(function(){
        notification_add('No se pudo conectar al servidor.','error');
        window['login']=0;
    });
    
}
function do_logout(){
    $.get($api+'logout',{user:checkCookie('user'),ssid:checkCookie('ssid')},function(result){
        var $result=JSON.parse(result);
        if($result['success']==true){
            resetLoginCookies();
            notification_add('Saliste correctamente.','success');
            check_login();
            load_view('home');
        }
    }).fail(function(){
        notification_add('No se pudo conectar al servidor.','error');
    });
}
function menu_show(){
    $('#header-nav').slideToggle();
    
}

function user_edit($id=null){
    load_view('user.edit');
    if($id!=null && checkCookie('type')!='admin'){
        notification_add('No tienes permisos para esto.','error');
        return null;
    }
    var $data={ssid:checkCookie('ssid'),id:$id};    
    $.get($api+'user',$data,function(result){
        var $result=JSON.parse(result);
        $('#name').val($result['name']);
        $('#phone').val($result['phone']);
        $('#email').val($result['email']);
        $('#id').val($result['id']);
    });
}
function user_save(){
    var $name=$('#name').val(),$phone=$('#phone').val(),$email=$('#email').val(),$id=$('#id').val(),$pass=null;
    if($name=='' || $phone=='' || $email==''){
        notification_add('Completa todos los campos','error');
        return null;
    }
    if($('#password').length>0){
        $pass=$('#password').val();
    }
    $.post($api+'user/save',{ssid:checkCookie('ssid'),name:$name,phone:$phone,email:$email,id:$id,password:$pass},function(result){
       var $result=JSON.parse(result);
       if($result['success']==true){
           if($('#header-logged-out').length>0){
                notification_add('Registro Exitoso.','success');
                load_view('user.login');
           }else{
                notification_add('Guardado correctamente.','success');
                if($id==null || $id==''){
                    user_list();
                }
           }
           
       }
    });
}
function user_list(){
    load_view('user.list');
    $.get($api+'user/list',{ssid:checkCookie('ssid')},function(result){
        var $result=JSON.parse(result);
        var $html='';
        for($z=0;$z<$result.length;$z++){
            $html+='<div class="Cuarto1"><div class="boton">'+$result[$z]['name']+'</div></div>';
            $html+='<div class="Cuarto1"><div class="boton">'+$result[$z]['phone']+'</div></div>';
            $html+='<div class="Cuarto1"><div class="boton">'+$result[$z]['email']+'</div></div>';
            $html+='<div class="Cuarto1"><div class="boton AzulOscuro" onclick="user_edit('+$result[$z]['id']+')">Editar</div></div>';
        }
        $('#list-content').html($html);
    });
}
function payment_all(){
    load_view('payment.list');
    $.get($api+'payment/list',{ssid:checkCookie('ssid')},function(result){
        var $result=JSON.parse(result);
        var $html='';
        for($z=0;$z<$result.length;$z++){
            $html+='<div class="Cuarto1"><div class="boton">'+$result[$z]['phone']+'</div></div>';
            $html+='<div class="Cuarto1"><div class="boton">'+$result[$z]['amount']+'</div></div>';
            $html+='<div class="Cuarto1"><div class="boton">'+$result[$z]['created_at']+'</div></div>';
            $html+='<div class="Cuarto1"><div class="boton AzulOscuro" onclick="payment_edit('+$result[$z]['id']+')">Editar</div></div>';
        }
        $('#list-content').html($html);
    });
}
function payment_edit($id){
    load_view('payment.edit');
    if($id && checkCookie('type')!='admin'){
        notification_add('No tienes permisos para esto.','error');
        return null;
    }
    var $data={ssid:checkCookie('ssid'),id:$id};    
    $.get($api+'payment',$data,function(result){
        var $result=JSON.parse(result);
        $('#phone').val($result['phone']);
        $('#amount').val($result['amount']);
        $('#id').val($result['id']);
    });
}
function payment_save(){
    var $amount=$('#amount').val(),$phone=$('#phone').val(),$id=$('#id').val();
    if($amount=='' || $phone==''){
        notification_add('Completa todos los campos','error');
        return null;
    }
    $.post($api+'payment/save',{ssid:checkCookie('ssid'),amount:$amount,phone:$phone,id:$id},function(result){
       var $result=JSON.parse(result);
       if($result['success']==true){
            notification_add('Guardado correctamente.','success');
       }
    });
}
function keepalive(){
    $.get($api+'time',{ssid:checkCookie('ssid')},function(result){
        var $result=JSON.parse(result);
        $('footer').data('time',$result['datetime-withoutseconds']);
    })
}
$(document).ready(function(){
    check_login();  
    setInterval(keepalive,60000);
});
$(window).resize(function(){
    if($(window).width()>=768){
        $('#header-nav').slideDown();
    }
})